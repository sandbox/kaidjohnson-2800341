<?php
/**
 * @file
 * Contains functions for configuring the Webform Constant Contact Subscribe
 * module
 */

require_once DRUPAL_ROOT . '/' . WEBFORM_CONSTANT_CONTACT_SUBSCRIBE_PATH . '/constant_contact/src/Ctct/autoload.php';

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

/**
 * Callback for drupal_get_form().
 * @see webform_constant_contact_subscribe_menu().
 */
function webform_constant_contact_subscribe_config_form($form, &$form_state) {
	$config = variable_get('webform_constant_contact_subscribe', array(
		'credentials' => array(
			'api_key' => '',
			'access_token' => '',
		),
		'lists' => array(),
	));

	if (!empty($config['credentials']['api_key']) && !empty($config['credentials']['access_token'])) {
		$cc = new ConstantContact($config['credentials']['api_key']);

		try {
			$lists = $cc->getLists($config['credentials']['access_token']);
		}
		catch (CtctException $ex) {
			foreach ($ex->getErrors() as $error) {
				drupal_set_message("$error[error_key]: $error[error_message]", 'error');
			}
		}
	}
	else {
		$lists = array();
	}

	$form['webform_constant_contact_subscribe'] = array(
		'#type' => 'container',
		'#tree' => TRUE,
	);

	$form['webform_constant_contact_subscribe']['credentials'] = array(
		'#type' => 'fieldset',
		'#title' => t('Constant Contact API Settings'),
		'#description' => t('In order to use this module, you will need to have a valid <b>api key</b> and <b>access token</b>. Both of these can be obtained from !link.', array(
			'!link' => l('http://constantcontact.mashery.com', 'http://constantcontact.mashery.com', array(
				'attributes' => array('target' => '_blank')
			)),
		)),
	);

	$form['webform_constant_contact_subscribe']['credentials']['api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('API Key'),
		'#default_value' => $config['credentials']['api_key'],
	);

	$form['webform_constant_contact_subscribe']['credentials']['access_token'] = array(
		'#type' => 'textfield',
		'#title' => t('Access Token'),
		'#default_value' => $config['credentials']['access_token'],
	);

	$form['webform_constant_contact_subscribe']['lists'] = array(
		'#type' => 'fieldset',
		'#title' => t('Lists available to use in webform options.'),
		'#tree' => TRUE,
	);

	foreach ($lists as $list) {
		$form['webform_constant_contact_subscribe']['lists'][$list->id] = array(
			'#type' => 'checkbox',
			'#title' => $list->name,
			'#default_value' => (empty($config['lists'][$list->id])) ? 0 : $list->name,
			'#return_value' => $list->name,
		);
	}

	return system_settings_form($form);
}
